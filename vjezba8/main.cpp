#include <iostream>
#include<vector>
using namespace std;
#include "structs.h";

int main() {
	int a = 5;
	int b = 3;

	string aStr = "aa";
	string bStr = "aaa";

	int minimalni;
	string minimalniStr;
	minimalni = minFun<int>(a, b);
	minimalniStr = minFun<string>(aStr, bStr);

	cout << minimalni << endl;
	cout << minimalniStr << endl;

	//string aStr = "test";
	//string bStr = "t";

	//string minimalni;
	//minimalni = minTemplate<string>(aStr, bStr);

	//cout << minimalni << endl;

	Skup<int> test;

	test.add(1);
	test.add(2);
	test.add(3);
	test.add(4);

	test.remove(2);

	test.print();

	if (test.check(3)) {
		cout << "Element pronaden" << endl;
	}
	else {
		cout << "Element nije pronaden" << endl;
	}


	Point <int> p1(2, 3), p2(3, 4);
	printf("udaljenost tocaka (%d, %d) i (%d, %d) je %f\n", p1.getA(), p1.getB(), p2.getA(), p2.getB(), p1 - p2);


	cout << endl;

	int niz[] = {2,5,4,7,1,9,6};
	int n = sizeof(niz) / sizeof(niz[0]);

	sortirajNiz(niz, n);

	for (int i = 0; i < n; i++) {
		cout << niz[i] << " ";
	}

	cout << endl;

	char niz2[] = {'b', 'd', 'c', 'E', 'e'};
	int n2 = sizeof(niz2) / sizeof(niz2[0]);

	sortirajNiz(niz2, n2);

	for (int i = 0; i < n2; i++) {
		cout << niz2[i] << " ";
	}
}
