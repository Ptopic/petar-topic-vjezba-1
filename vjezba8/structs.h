﻿#pragma once
#include <string>
#include <algorithm>
#include <vector>
#include <math.h>
#include <cmath>

/*

1. Napisati template funkciju min koja vraća manji od dva elementa. 
Elementi mogu biti brojevi i stringovi.

*/

template <class T> 
int minFun(int a, int b)
{
	if (a < b) {
		return a;
	}
	return b;
}

template <class T>
string minFun(string a, string b)
{
	if (a < b) {
		return a;
	}
	return b;
}

/*

2. Definirajte template klasu za skup. U skup se mogu dodavati elementi,
izbacivati elementi i može se provjeriti je li neki element u skupu ili ne.

*/

template<typename T>
class Skup {
	vector<T> n;
	public:
		Skup() {
			n = {0};
		}

		Skup(vector<T> nV) {
			n = nV;
		}

		void add(T br) {
			n.push_back(br);
		}

		void remove(T i) {
			n.erase(n.begin() + i);
		}

		bool check(T br) {
			for (int i = 0; i < n.size(); i++) {
				if (n.at(i) == br) {
					return true;
				}
			}
			return false;
		}
		void print() {
			for (int i = 0; i < n.size(); i++) {
				cout << n.at(i) << endl;
			}
		}

};

/*

3. Napišite template funkciju za sortiranje niza. 
Napišite specijalizaciju te funkcije za niz charova u kojoj neće biti razlike između malih i velikih slova.

*/

template<typename T>
void sortirajNiz(T arr[], int SIZE) {
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = i + 1; j < SIZE; j++)
		{
			if (arr[i] > arr[j])
			{
				T temp;
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
}


template <>
void sortirajNiz(char arr[], int SIZE) {
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = i + 1; j < SIZE; j++)
		{
			if (tolower(arr[i]) > tolower(arr[j]))
			{
				char temp;
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

/*

Napišite template klasu point i potrebne operatore tako da sljedeći program

point <int> p1 (2 ,3) , p2 (3 ,4) ;
cout << " udaljenost tocaka " << p1 << " i " << p2 << " je " << p1 - p2 << endl;

pravilno ispisuje udaljenost među zadanim točkama. Udaljenost točaka (2,3) i (3,4) je 1.41421.
*/


template<typename T>
class Point {
	T a;
	T b;
public:
	Point() {
		a = 0;
		b = 0;
	}

	Point(T aV, T bV) {
		a = aV;
		b = bV;
	}

	T getA() {
		return a;
	}

	T getB() {
		return b;
	}

	float operator -(Point p2)
	{
		return sqrt(pow(p2.a - a, 2) + pow(p2.b - b, 2));
	}
};
