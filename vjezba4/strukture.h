#pragma once
#include <iostream>
#include<vector>
using namespace std;

class Point {
public: 
	double x = 1.0;
	double y = 1.0;

	Point() {};

	Point(double xv, double yv) { x = xv; y = yv; };
};

class Board {
public:
	int rows = 2;
	int cols = 2;
	char** arr;
	char ch = 'o';
	// Default konstruktor
	Board() {
		arr = new char* [rows];

		for (int i = 0; i < rows; i++) {
			arr[i] = new char[cols];
			if (i == 0 || i == rows - 1) {
				for (int j = 0; j < cols; j++) {
					arr[i][j] = ch;

				}
			}
			else {
				arr[i][0] = ch;
				arr[i][cols - 1] = ch;
			}

		}
	};

	// Konstruktor s argumentima
	Board(int rowv, int colv) {
		rows = rowv;
		cols = colv;
		
		arr = new char* [rows];

		for (int i = 0; i < rows; i++) {
			arr[i] = new char[cols];
			if (i == 0 || i == rows-1) {
				for (int j = 0; j < cols; j++) {
					arr[i][j] = ch;

				}
			}
			else {
				arr[i][0] = ch;
				arr[i][cols-1] = ch;
			}

		}
	}

	// Copy konstruktor
	Board(const Board& other);

	// Move konstruktor
	Board(Board&& other);

	//// Destruktor
	~Board() {
		cout << "Destructor" << endl;
	}

	void bresenham(int x1, int y1, int x2, int y2, char ch)
	{
		int m_new = 2 * (y2 - y1);

		int slope_error_new = m_new - (x2 - x1);

		for (int x = x1, y = y1; x <= x2; x++) {
			cout << "(" << x << "," << y << ")\n";
			Point p(x, y);
			draw_char(p, ch);

			slope_error_new += m_new;

			if (slope_error_new >= 0) {
				y++;
				slope_error_new -= 2 * (x2 - x1);
			}
		}
	}

	void draw_char(Point p, char ch) {
		int x = p.x;
		int y = p.y;

		arr[y][x] = ch;
	};

	void draw_up_line(Point p, char ch) {
		int x = (int)round(p.x);
		int y = (int)round(p.y);

		for (int i = x; i > 0; i--) {
			arr[i][y] = ch;
		}


	};

	void draw_line(Point p1, Point p2, char ch) {
		int x1 = (int)round(p1.x);
		int y1 = (int)round(p1.y);

		int x2 = (int)round(p2.x);
		int y2 = (int)round(p2.y);

		bresenham(x1, y1, x2, y2, ch);
		//if (x1 != x2 && y1 != y2) {
		//	int i = x1;
		//	int j = y1;
		//	for (i = x1, j = y1; i < x2; i++, j++) {
		//		arr[i][j] = ch;
		//	}
		//}
		//else if (x1 < x2 && y1 < y2) {
		//	int i = x1;
		//	int j = y1;
		//	for (i = x1, j = y1; i < x2; i++, j++) {
		//		arr[i][j] = ch;
		//	}
		//}
		//// y jednaki xovi razliciti (ravna crta)
		//else if (x1 < x2 && y1 == y2) {
		//	int i = x1;
		//	int j = y1;
		//	for (i = x1, j = y1; i < x2; i++) {
		//		arr[j][i] = ch;
		//	}
		//}
		//// y jednaki xovi razliciti (ravna crta)
		//else if (x1 > x2 && y1 == y2) {
		//	int i = x1;
		//	int j = y1;
		//	for (i = x2, j = y1; i < x1; i++) {
		//		arr[j][i] = ch;
		//	}
		//}
		//// x ovi jednaki y razliciti (Ravna crta put gore)
		//else if (x1 == x2 && y2 > y1) {
		//	int i = x1;
		//	int j = y2;
		//	for (i = y1, j = y1; j < y2; j++) {
		//		arr[j][i] = ch;
		//	}
		//}

		//// x ovi jednaki y razliciti (Ravna crta put gore)
		//else if (x1 == x2 && y2 < y1) {
		//	int i = x1;
		//	int j = y2;
		//	for (i = x1, j = y2; j < y1; j++) {
		//		arr[j][i] = ch;
		//	}
		//}
	};

	void display() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				cout << arr[i][j] << " ";
			}
			cout << endl;
		}
	}
};

Board::Board(const Board& other) : arr(other.arr), ch(other.ch) {};
Board::Board(Board&& other) : arr(other.arr), ch(other.ch) {};



