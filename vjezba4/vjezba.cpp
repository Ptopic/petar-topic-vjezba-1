#include <iostream>
#include<vector>
using namespace std;
#include "strukture.h";

int main()
{
    Board b(10, 20);
    Point p1(2, 2), p2(8, 8), p3(2, 8), p4(16, 8);

    // x1 < x2 - mora bit

    b.draw_line(p1, p2, 'x');

    b.draw_line(p3, p4, 'x');

    b.display();
}

