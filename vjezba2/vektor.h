#include <cstddef>
#include <iostream>
#include <vector>
using namespace std;
using std::vector;

void unosVektor1(vector<int> &vektor, int size);

void unosVektor2(vector<int> &vektor, int size, int donjaGranica, int gornjaGranica);

void ispisVektor(vector<int> &vektor);

void sortVektor(vector<int>& vektor);