﻿#include "vektor.h"
#include <algorithm>

//1. Napisati funkcije za unos i ispis vektora brojeva.Prototipove funkcija staviti u
//	.hpp datoteke, a implementaciju u.cpp datoteke.
//	Za unos vektora napisati dvije funkcije :
// 
//	funkcija čiji su parametri referenca na vektor i broj elemenata vektora,
//	
//  funkcija čiji su parametri referenca na vektor i granice intervala u kojem
//	trebaju biti elementi vektora.
//  Elementi vektora se unose u funkciji, sve dok
//	se ne unese broj koji nije u danom intervalu.

void unosVektor1(vector<int> &vektor, int size) {
	int el;
	cout << "Unesite elemente niza" << endl;
	for (int i = 0; i < size; i++) {
		cin >> el;
		vektor.push_back(el);
	}
};

void unosVektor2(vector<int> &vektor, int size, int donjaGranica, int gornjaGranica) {
	int input;
	printf("Unesite elemente u intervalu [%d, %d] \n", donjaGranica, gornjaGranica);
	for (int i = 0; i < size; i++) {
		cin >> input;
		if (input < donjaGranica || input > gornjaGranica) {
			break;
		}
		else {
			vektor.push_back(input);
		}
	}

};

void ispisVektor(vector<int> &vektor) {
	cout << "Ispis vektora: " << endl;
	for (int i = 0; i < vektor.size(); i++) {
		cout << vektor.at(i) << endl;
	}

};

/*2. Koristeći STL funkcije sortirajte vektor,
ubacite 0 ispred najmanjeg elementa, te
sumu svih elemenata iza najvećeg elementa.*/
void sortVektor(vector<int>& vektor) {
	sort(vektor.begin(), vektor.end());
	ispisVektor(vektor);
	// Sortiran je vektor pa je 0 uvik najmanji indeks, i zadnji je najveci
	int max = vektor.size(), sum = 0;
	for (int i = 0; i < vektor.size(); i++) {
		// Add to sum
		sum += vektor.at(i);
	}

	cout << sum << endl;

	//vektor.resize(vektor.size() + 2);
	vektor.insert(vektor.begin(), 0);
	vektor.push_back(sum);
	
	cout << "print novog vektora" << endl;
	ispisVektor(vektor);
}

