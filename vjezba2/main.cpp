﻿#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <cstring>
#include <time.h>
#include "vektor.h"
using namespace std;

/* 3. Napisati funkciju koja broji koliko pojavljivanja danog podstringa ima u stringu koristeći funkcije standardne biblioteke. */
int funkcija3(string str, string substr) {
	int count = 0;

	int found = str.find(substr, 0);
	while (found != string::npos) {
		count++;
		found = str.find(substr, found + 1);
	}

	return count;
}

/* 4. Napišite funkciju koja stringove, koje korisnik unosi, sprema u vektor stingova,
svaki string preokrene 
te sortira vektor po preokrenutim stringovima. */
void funkcija4(vector<string>& vektor) {
	string str;
	for (int i = 0; i < vektor.size(); i++) {
		cout << "Enter a string" << endl;
		getline(cin, str);

		// Reverse string
		reverse(str.begin(), str.end());
		vektor.at(i) = str;
	}

	sort(vektor.begin(), vektor.end());

	for (int i = 0; i < vektor.size(); i++) {
		cout << vektor.at(i) << endl;
	}
}


/* 5. Učitati string koji predstavlja rečenicu.
Napisati funkciju koja iz stringa izbacuje sve praznine koje se nalaze ispred znakova interpunkcije 
i dodaje praznine nakon znaka interpunkcije ako nedostaju. 

Primjer: Za rečenicu ”Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .”,
Ispravna rečenica glasi: ”Ja bih, ako ikako mogu, ovu recenicu napisala ispravno.”.
*/

bool isPunctuation(char c) {
	return ((c == '.') || (c == ',') || (c == '?') || (c == '!'));
}

string funkcija5(string str) {
	string res = "";
	int len = str.length();
	char tempBefore = ' ', tempAfter = ' ';

	// Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .

	// Trim recenicu

	int i = 0, j = -1;

	bool spaceFound = false;

	while (++j < len && str[j] == ' ');

	while (j < len)
	{
		if (str[j] != ' ')
		{
			if ((str[j] == '.' || str[j] == ',' ||
				str[j] == '?') && i - 1 >= 0 &&
				str[i - 1] == ' ')
				str[i - 1] = str[j++];

			else
				str[i++] = str[j++];

			spaceFound = false;
		}
		else if (str[j++] == ' ')
		{
			if (!spaceFound)
			{
				str[i++] = ' ';
				spaceFound = true;
			}
		}
	}

	if (i <= 1) {
		str.erase(str.begin() + i, str.end());
	}
	else {
		str.erase(str.begin() + i - 1, str.end());
	}

	len = str.length();

	// Obrtanje zareza i praznog mista

	for (int i = 0; i < len; i++) {
		if (isPunctuation(str[i])) {
			if (i == 0) {
				tempAfter = str[i + 1];
				if (tempAfter != ' ') {
					str.insert(i + 1, " ");
				}
			}

			else if (i == (len - 1)) {
				tempBefore = str[i - 1];
				if (tempBefore == ' ') {
					str.erase(str.begin() + i - 1);
				}
			}

			else {
				tempBefore = str[i - 1];
				tempAfter = str[i + 1];

				if (tempAfter != ' ') {
					str.insert(i + 1, " ");
				}

				if (tempBefore == ' ') {
					str.erase(str.begin() + i - 1);
				}
			}
		}

	}

	return str;
}


/* 6. Napisati funkciju koji prevodi engleske rečenice na pig latin jezik.Pravila su sljedeća :
(a)ako riječ počinje samoglasnikom dopisuje se hay na kraj riječi,
(b)inače, svi suglasnici s početka riječi prebacuju se na kraj te se na kraju riječi
dopisuje ay.

Rečenice treba spremiti u vector, a zatim prevesti slučajno odabranu rečenicu iz
vektora. 

Primjer: ”What time is it?” prevodi se kao ”atwhay imetay ishay ithay?”
*/

bool isVowel(char c) {
	return (c == 'A' || c == 'E' || c == 'I' ||
		c == 'O' || c == 'U' || c == 'a' ||
		c == 'e' || c == 'i' || c == 'o' ||
		c == 'u');
}
string pigLat(string word) {
	int len = word.length();
	int index = -1;
	string tempStr;
	char lastLetter = ' ';
	char puncSign = ' ';

	// Spremi ? ! . u zasebnu varijablu i makni ih iz rici te ih dodaju na kraj rici ako postoji
	if (isPunctuation(word.back())) {
		cout << "punc" << endl;

		puncSign = word.back();
		word.pop_back();

	}

	for (int i = 0; i < len; i++) {
		if (isVowel(word[i])) {
			index = i;
			break;
		}
	}

	if (index == 0) {
		// ako riječ počinje samoglasnikom dopisuje se hay na kraj riječi
		if (puncSign != ' ') {
			return word.substr(index) + "hay" + puncSign;
		}
		else {
			return word.substr(index) + "hay";
		}

	}
	else {

		// what --- atwhay
		// svi suglasnici s početka riječi prebacuju se na kraj te se na kraju riječi dopisuje ay
		// index = 2
		// Od indexa do kraja
		// cout << word.substr(index) << endl;
		// od pocetka do indexa (ostatak koji nisu samoglasnici)
		// cout << word.substr(0, index) << endl;

		/*cout << word.substr(index).back() << endl;*/
		tempStr = word.substr(index) + word.substr(0, index) + "ay";
		

	}

	/*tempStr.push_back('?');*/
	if (puncSign != ' ') {
		return tempStr + puncSign;
	}
	else {
		return tempStr;
	}

	// Ako nema samoglasnika ne moze se izvest
	if (index == -1) {
		return word;
	}
}

string pigLatRecenica(string recenica) {
	string word = "";
	string novaRecenica = "";
	char lastLetter = ' ';

	for (auto x : recenica)
	{
		if (x == ' ')
		{
			string str = pigLat(word);
			novaRecenica += str + ' ';
			word = "";
		}
		else {
			word = word + x;
		}
	}
	string str = pigLat(word);
	novaRecenica += str + ' ';



	return novaRecenica;
}


int main() {
	//cout << "Unesite velicinu vektora: " << endl;

	//int size;
	//cin >> size;
	//
	//vector<int>vektor;

	//unosVektor1(vektor, size);
	////unosVektor2(vektor, size, 3,7); 
	//ispisVektor(vektor);
	//sortVektor(vektor);



	// 3. zad
	//string str= "There are two needles in this haystack with needles.";

	//string substr = "needle";

	//int res = funkcija3(str, substr);

	//cout << res << endl;

	// 4. zad
	//int n = 3;
	//vector<string> vektor(n);
	//funkcija4(vektor);

	// 5. zad --- Dovrsit
	string str = "Ja bih  , ako ikako mogu ,  ovu recenicu napisala ispravno. ";

	string res = "";
	res = funkcija5(str);

	cout << res << endl;

	// 6. zad
	// Rečenice treba spremiti u vector, a zatim prevesti slučajno odabranu rečenicu iz vektora.

	//vector<string>recenice = { "What time is it?", "What time it is?", "it is time for programming?" };

	//srand(time(NULL));

	//int randInt;
	//randInt = rand() % recenice.size();

	//cout << randInt << endl;
	//string novaRecenica = "";
	//novaRecenica = pigLatRecenica(recenice.at(randInt));
	//cout << novaRecenica << endl;


}