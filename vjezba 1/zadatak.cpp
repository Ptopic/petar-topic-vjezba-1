﻿#include <iostream>
#include "vector.hpp"

//1. Napisati funkciju koja vraća referencu na neki prvi element niza koji je veći
//od nule.Koristeći povratnu vrijednost funkcije kao lvalue u main funkciji
//promijenite vrijednost tog elementa na nula.

int& funkcija1(int* niz, int size) {
	int i;
	for (i = 0; i < size; i++) {
		if (niz[i] > 0) {
			return niz[i];
		}
	}
}

//2. Napisati funkciju koja prima niz brojeva i broj elemenata niza te 

// iz niza izbacuje duplikate, pri čemu se broj elemenata treba promijeniti.

// U main funkciji korisnik unosi inicijalni broj elemenata niza.

// Napisati funkcije za unos niza i ispisivanje niza.

void unosNiza(int arr[], int size) {
	for (int i = 0; i < size - 1; i++) {
		std::cin >> arr[i];
	}
}

void ispisNiza(int arr[], int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i];
	}
}

void funkcija2(int arr[], int size) {
	// niz -> [1,2,2,3,3,3,4,5,5,6]
			// 1 pa petlja koja gleda jel ima u ostatku 1 koja se ponavlja ako nema pusha ga u drugi nit
			// 2 ako ima 
	int temp[100];

	int k = 0;
	for (int i = 0; i < size; i++) {
	
		int flag = 1;
		for (int j = 0; j < i; j++) {
			if (arr[i] == arr[j]) {
				// Ako postoji ponavljanje prije trenutne pozicije u nizu (i) prekidamo petlju i ne spremamo ga u temp niz
				flag = 0;
				break;
			}
		}

		if (flag) {
			temp[k] = arr[i];
			k++;
		}
	}

	ispisNiza(temp, k);
}


//3. Napisati funkciju koja vraća niz int vrijednosti veličine n u kojem je svaki
//element zbroj svoja dva prethodnika.
// Prvi i drugi element u nizu su jedinice. U main funkciji ispisati dobiveni niz i osloboditi memoriju.
// n = 4
// 


void funkcija3(int* arr, int size) {
	if (size >= 2) {
		arr[0] = 1;
		arr[1] = 1;
	}
	int i;
	for (i = 2; i < size; i++) {
		// [1, 1, 1+1, 2+1]
		arr[i] = (arr[i - 1] + arr[i - 2]);
		std::cout << arr[i];

	}
	std::cout << "break \n";
}


//4. Definirati strukturu koja opisuje vektor.
// Struktura se sastoji od niza int elemenata, logičke i fizičke veličine niza.Fizička veličina je inicijalno init, a
//kada se ta veličina napuni vrijednostima, alocira se duplo.
//Napisati funkcije vector_new, vector_delete, vector_push_back, vector_pop_back,
//vector_front, vector_back i vector_size.Funkcije su članovi strukture

int main() {
	// --- 1 ---

	int niz[] = { 0, 1, 2, 3 };
	int size = sizeof(niz) / sizeof(niz[0]);
	funkcija1(niz, size) = 0;
	ispisNiza(niz, size);



	// --- 2 ---

	int n;
	std::cout << "Unesite velicin uniza ";
	std::cin >> n;

	int* arr;
	arr = new int[n];

	std::cout << "Unesite elemente niza \n";

	for (int m = 0; m < n; m++) {
		std::cin >> arr[m];
	}

	funkcija2(arr, n);
	


	// --- 3 ---

	//int n = 7;
	//int arr[100];
	//funkcija3(arr, n);

	//for (int i = 0; i < n; i++) {
	//	std::cout << arr[i] << "\n";
	//}

}