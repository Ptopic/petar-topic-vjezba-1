#include <iostream>
#include "vector.hpp"

using namespace std;
int main() 
{
    MyVector mv;

    mv.vector_new(5);
    
    int m;
	
    std::cout << "Unesi element, Ctrl+d (linux) ili Ctrl+z (win) za kraj unosa" << endl;

    while(std::cin >> m)
        mv.vector_push_back(m);
    

	std::cout << "size " << mv.vector_size();
	std::cout << "first element " << mv.vector_front() << endl;
	std::cout << "last element " << mv.vector_back() << endl;
    mv.print_vector();
	
	std::cout << "removing last element" << endl;
    mv.vector_pop_back();
    mv.print_vector();
	
	std::cout << "capacity " << mv.capacity << endl;

    mv.vector_delete();
}
