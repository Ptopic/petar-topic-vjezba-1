﻿#include <iostream>
#include<vector>
using namespace std;
#include "strukture.h";
#include <ctime>
#include <exception>
// for <streamsize>
#include <ios>
#include<string>

// for numeric_limits
#include <limits>

class NotNumber : public std::exception
{	
public:
	string msg = "Nije unesen broj";
};

class NotOperator : public std::exception
{
public:
		string msg = "Nije unesen jedan od podrzanih (+-*/) operatora";
};

class DevideZero : public std::exception
{
public:
	string msg = "Djeljenje sa nulom";
};


// funkciju za unos jednog cijelog broja - baca iznimku ako nije unesen broj;

int unesiBroj() {
	int br; 
	cout << "Unesi broj " << endl;
	cin >> br;	
	if (cin.fail()) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw NotNumber();
	}
	

	return br;
}


// funkciju za unos operatora - baca iznimku ako operator nije jedan od podržanih (+-*/)
char unosOperatora() {
	cout << "Unesi operator " << endl;
	char ch; cin >> ch;
	if (ch != '+' && ch != '-' && ch != '*' && ch != '/') {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw NotOperator();
	}

	return ch;
}


// funkciju koja računa rezultat operacije - baca iznimku u slučaju problema (npr.dijeljenje s nulom)

int izracunOperacija(int br1, int br2, char operacija) {
	if (operacija == '+') {
		return br1 + br2;
	}
	else if (operacija == '-') {
		return br1 - br2;
	}
	else if (operacija == '*') {
		return br1 * br2;
	}
	else if (operacija == '/') {
		if (br2 == 0) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			throw DevideZero();
		}

		return br1 / br2;
	}
}


int main()
{	
	vector<string> exceptions;

	while (1) {
		time_t now = time(0);
		string dt = ctime(&now);
		string msg;

		

		try {
			int br1 = unesiBroj();
			if (br1 == 0) {
				cout << "Print svih gresaka" << endl;
				for (int i = 0; i < exceptions.size(); i++) {
					std::cout << exceptions.at(i);
				}
				break;
			}
			int br2 = unesiBroj();

			char ch = unosOperatora();


			int res = izracunOperacija(br1, br2, ch);
			cout << br1 << ch << br2 << "=" << res << endl;
		}
		catch (NotNumber n) {
			msg = n.msg;
			string errorMsg = dt + msg;
			exceptions.push_back(errorMsg);
		}
		catch (NotOperator n) {
			msg = n.msg;
			string errorMsg = dt + msg;
			exceptions.push_back(errorMsg);
		}
		catch (DevideZero n) {
			msg = n.msg;
			string errorMsg = dt + msg;
			exceptions.push_back(errorMsg);
		}
	}



	

}

