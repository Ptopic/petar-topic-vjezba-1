﻿#pragma once
#include <iostream>
#include<vector>
using namespace std;

class Vec3 {
public:
	double x;
	double y;
	double z;
	// Default konstruktor
	Vec3() {
		x = 0;
		y = 0;
		z = 0;
	};


	// Konstruktor s argumentima
	Vec3(double xv, double yv, double zv) {
		x = xv;
		y = yv;
		z = zv;
	}

	//// Destruktor
	//~Vec3();

	// Unos
	void unos() {
		cout << "Unesite x kordinatu vektora" << endl;
		cin >> x;

		cout << "Unesite y kordinatu vektora" << endl;
		cin >> y;

		cout << "Unesite z kordinatu vektora" << endl;
		cin >> z;

		cout << endl;
	}


	// Ispis
	void print() {
		printf("(%.2f x, %.2f y, %.2f z)", x, y, z);
		cout << endl;
	}

	// Operator =
	void equal(char ch, double broj) {
		if (ch == 'x' || ch == 'X') {
			x = broj;
		}
		else if (ch = 'y' || ch == 'Y') {
			y = broj;
		}
		else if(ch = 'z' || ch == 'Z') {
			z = broj;
		}
		else {
			cout << "Greska" << endl;
		}
	}

	// operatore +=, -=, *=, /=,
	void equalPlus (char ch, double broj) {
		if (ch == 'x' || ch == 'X') {
			x = x + broj;
		}
		else if (ch = 'y' || ch == 'Y') {
			y = y + broj;
		}
		else if (ch = 'z' || ch == 'Z') {
			z = z + broj;
		}
		else {
			cout << "Greska" << endl;
		}
	}

	void equalMinus(char ch, double broj) {
		if (ch == 'x' || ch == 'X') {
			x = x - broj;
		}
		else if (ch = 'y' || ch == 'Y') {
			y = y - broj;
		}
		else if (ch = 'z' || ch == 'Z') {
			z = z - broj;
		}
		else {
			cout << "Greska" << endl;
		}
	}

	void equalMultiply(char ch, double broj) {
		if (ch == 'x' || ch == 'X') {
			x =  x * broj;
		}
		else if (ch = 'y' || ch == 'Y') {
			y = y * broj;
		}
		else if (ch = 'z' || ch == 'Z') {
			z = z * broj;
		}
		else {
			cout << "Greska" << endl;
		}
	}

	void equalDevide(char ch, double broj) {
		if (ch == 'x' || ch == 'X') {
			x =  x / broj;
		}
		else if (ch = 'y' || ch == 'Y') {
			y = y / broj;
		}
		else if (ch = 'z' || ch == 'Z') {
			z = z / broj;
		}
		else {
			cout << "Greska" << endl;
		}
	}

	// operatore + i - za zbrajanje i oduzimanje vektora,
	Vec3 zbroji(Vec3 vektor, Vec3 vektor2) {
		Vec3 res;

		res.x = vektor.x + vektor2.x;
		res.y = vektor.y + vektor2.y;
		res.z = vektor.z + vektor2.z;

		res.print();

		
		return res;
	}

	Vec3 oduzmi(Vec3 vektor, Vec3 vektor2) {
		Vec3 res;

		// Myb fix oduzimanje da ne bude negativna vrjednost ako je prvi vektor manji od drugog
		res.x = vektor.x - vektor2.x;
		res.y = vektor.y - vektor2.y;
		res.z = vektor.z - vektor2.z;

		res.print();


		return res;
	}

	// operatore * i / za množenje i dijeljenje vektora sa skalarom
	void pomnoziSkalarom(int skalar) {
		x = x * skalar;
		y = y * skalar;
		z = z * skalar;
	}

	void podjeliSkalarom(int skalar) {
		x = x / skalar;
		y = y / skalar;
		z = z / skalar;
	}

	// operatore jednakosti i nejednakosti (član po član),
	int jednaki(Vec3 vektor1, Vec3 vektor2) {
		if (vektor1.x == vektor2.x && vektor1.y == vektor2.y && vektor1.z == vektor2.z) {
			return 1;
		}
		else {
			return 0;
		}
	}

	// operator *, za skalarni produkt,
	double skalarniProdukt(Vec3 vektor1, Vec3 vektor2) {
		double res = (vektor1.x * vektor2.x) + (vektor1.y * vektor2.y) + (vektor1.z * vektor2.z);
		return res;
	}

	// operatore [], kojem se pristupa pojedinoj koordinati vektora.
	double getCord(char ch) {
		if (ch == 'x' || ch == 'X') {
			return x;
		}
		else if (ch = 'y' || ch == 'Y') {
			return y;
		}
		else if (ch = 'z' || ch == 'Z') {
			return z;
		}
		else {
			cout << "Greska" << endl;
			return 0;
		}
	}

	// Funkcija koja normalizira vektor
	void normalizirajVektor() {
		double w = sqrt(x * x + y * y + z * z);
		x /= w;
		y /= w;
		z /= w;
	}

};



