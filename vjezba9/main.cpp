﻿#include <iostream>
#include<vector>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <exception>
using namespace std;

/*
1. Napisati funkciju koja iz datoteke numbers.txt čita prirodne brojeve.Koristeći standardne algoritme :

Za učitavanje iz datoteke i ispisivanje na konzolu koristite copy.
*/

class NoFile : public std::exception
{
public:
	string msg = "Ne postoji datoteka";
};



void print(vector<int> v)
{
	for (int i = 0; i < v.size(); i++) {
		cout << v.at(i) << endl;
	}
}

vector<int> funkcija()
{
	ifstream file;
	string line;
	vector<int>numbers;
	int num;
	file.open("numbers.txt");

	if (file.is_open())
	{
		while (getline(file, line))
		{
			// (b)napuni vektor brojevima iz datoteke,
			num = atoi(line.c_str());
			numbers.push_back(num);
		}
		file.close();

		// (c)prebroji sve brojeve vece od 500,
		int count = 0;
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.at(i) > 500)
			{
				count++;
			}
		}

		printf("Brojevi veci od 500 - %d\n", count);

		// (d)nađi minimalni i maximalni element,

		int min = *min_element(numbers.begin(), numbers.end());
		int max = *max_element(numbers.begin(), numbers.end());
		cout << "min element " << min << endl;
		cout << "max element " << max << endl;

		// (e)iz vektora izbaci brojeve manje od 300,

		vector<int>erased;
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.at(i) > 300)
			{
				erased.push_back(numbers.at(i));
			}

		}

		print(erased);

		// (f)preostale brojeve sortiraj silazno.

		cout << "Sortiran" << endl;

		sort(erased.begin(), erased.end(), greater<int>());

		print(erased);

		return erased;
	}
	else {
		// Throw exception
		// (a)baci iznimku ako ne postoji datoteka,
		throw NoFile();
	}
}


bool neparan(int i)
{
	return ((i % 2) == 1);
}

bool potencija(int i)
{
	while (i % 2 == 0) {
		i = i / 2;
	}

	// check if n is a power of 2
	if (i == 1) {
		return true;
	}
	else {
		return false;
	}
}

int main()
{
	vector<int> v;
	try {
		v = funkcija();
	}
	catch (NoFile n)
	{
		string msg = n.msg;
		cout << msg << endl;
	}

	// *2. U vektoru koristeći standardne algoritme :

	// 	(a) pronađite prvi neparni broj ako postoji (find_if)
	vector<int>::iterator it = find_if(v.begin(), v.end(), neparan);

	cout << "Prvi neparni broj " << *it << '\n';

	// 	(b) pronađite ukupan broj neparnih brojeva (count_if)

	int mycount = count_if(v.begin(), v.end(), neparan);

	cout << "Broj neparnih brojeva " << mycount << '\n';
	
	// 	(c) zamijenite sve potencije broja 2 sa 2 (replace_if)

	replace_if(v.begin(), v.end(), potencija, 2);

	print(v);

	// (d)ispišite prvo sve parne brojeve od manjeg ka većem, zatim sve neparne od manjeg ka većem(sort)

	sort(v.begin(), v.end());

	print(v);

	cout << "parni sortirani " << endl;
	for (int i = 0; i < v.size(); i++) {
		if (v.at(i) % 2 == 0) {
			cout << v.at(i) << endl;
		}
	}

	cout << "neparni sortirani " << endl;
	for (int i = 0; i < v.size(); i++) {
		if (v.at(i) % 2 != 0) {
			cout << v.at(i) << endl;
		}
	}
}