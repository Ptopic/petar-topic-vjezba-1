﻿#pragma once
#include <iostream>
#include<vector>
#include <string>
using namespace std;

class Zivotinja {
	public:
		virtual int returnBrojNogu() = 0;
		virtual string returnString() = 0;
};

class Kukac : public Zivotinja {
	public:
		int returnBrojNogu() {
			return 0;
		}
		string returnString() 
		{
			return "Kukac";
		}
};

class Zohar : public Zivotinja {
	public:
		int returnBrojNogu() override {
			return 6;
		}
		string returnString() override {
			return "Zohar";
		}
};


class Pauk : public Zivotinja {
	public:
		int returnBrojNogu() {
			return 0;
		}
		string returnString()
		{
			return "Pauk";
		}
};

class Tarantula : public Zivotinja {
public:
	int returnBrojNogu()override {
		return 8;
	}
	string returnString() override
	{
		return "Tarantula";
	}
};


class Ptica : public Zivotinja {
	public:
		int returnBrojNogu() {
			return 0;
		}
		string returnString()
		{
			return "Ptica";
		}
};

class Vrabac : public Zivotinja{
public:
	int returnBrojNogu() override {
		return 2;
	}
	string returnString() override
	{
		return "Vrabac";
	}
};

class Brojac {
	public:
		int count = 0;

		void add(Zivotinja* zivotinja) {
			int res = zivotinja->returnBrojNogu();
			cout << res << endl;
			count = count + res;
			// Ispis zivotinje koja je dodana

			string vrsta = zivotinja->returnString();
			cout << "Dodana: " << vrsta << endl;
		}

		void printCount() {
			cout << "Ukupno nogu " << count << endl;
		}
};


