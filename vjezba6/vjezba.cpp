#include <iostream>
#include<vector>
using namespace std;
#include "strukture.h";

int main()
{
	int res = 0;
	Tarantula t;
	res = t.returnBrojNogu();

	cout << res << endl;

	Zohar z;
	res = z.returnBrojNogu();

	cout << res << endl;

	Vrabac v;
	res = v.returnBrojNogu();

	cout << res << endl;

	Brojac brojac;

	brojac.add(&v);
	brojac.add(&t);
	brojac.add(&z);

	brojac.printCount();

}

