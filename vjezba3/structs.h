#pragma once
#include <iostream>
using namespace std;

struct tocka {
    double duzina;
    double sirina;
    double visina;

    void set(double x = 0, double y = 0, double z = 0);
    void setRandom(int gornjaGranica, int donjaGranica);
    const void get(double* x, double* y, double* z);
    double udaljenost2D(tocka druga);
    double udaljenost3D(tocka druga);
};



struct oruzje {
    tocka polozaj;
    int metciKapactite;
    int curMetci = 0;

    void shoot();
    void reload();
};



struct meta {
    tocka donja;
    tocka gornja;
    bool pogodak(oruzje oruzje);
};