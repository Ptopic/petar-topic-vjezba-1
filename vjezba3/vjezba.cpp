﻿#include <iostream>
#include "structs.h"
using namespace std;


int main()
{
    // Zadatak 1
    tocka tocka1;
    tocka tocka2;

    tocka1.set(1.2, 7.2, 3.2);
    tocka2.set(12, 4.2, 24);

    cout << tocka1.duzina << endl;
    cout << tocka1.sirina << endl;
    cout << tocka1.visina << endl;

    // Ispisi udaljenosti
    int udaljenost2D = tocka1.udaljenost2D(tocka2);
    cout << "udaljenost " << udaljenost2D << endl;

    int udaljenost3D = tocka1.udaljenost2D(tocka2);
    cout << "udaljenost " << udaljenost2D << endl;


    // Zadatak 2
    oruzje pistolj;

    tocka tockaPolozaja= { 1,2,3 };
    pistolj.polozaj = tockaPolozaja;
    pistolj.metciKapactite = 10;


    cout << "Broj metaka " << pistolj.curMetci << endl;
    pistolj.reload();
    cout << "Broj metaka " << pistolj.curMetci << endl;
    pistolj.shoot();
    cout << "Broj metaka " << pistolj.curMetci << endl;


    // Zadatak 4

    int n = 3;

    int gornjaGranica = 10, donjaGranica = 0;
    // Generiraj n meta
    meta* mete = (meta*)malloc(sizeof(meta) * n);
    for (int i = 0; i < n; i++) {
        meta curMeta;
        double duzina1 = 0, sirina1 = 0, visina1 = 0;
        double duzina2 = 0, sirina2 = 0, visina2 = 0;
        duzina1 = donjaGranica + rand() % (gornjaGranica - donjaGranica + 1);
        sirina1 = donjaGranica + rand() % (gornjaGranica - donjaGranica + 1);
        visina1 = donjaGranica + rand() % (gornjaGranica - donjaGranica + 1);


        duzina2 = donjaGranica + rand() % (gornjaGranica - donjaGranica + 1);
        sirina2 = donjaGranica + rand() % (gornjaGranica - donjaGranica + 1);
        visina2 = donjaGranica + rand() % (gornjaGranica - donjaGranica + 1);

        if (visina2 > visina1) {
            curMeta.gornja.set(duzina1, sirina1, visina1);
            curMeta.donja.set(duzina2, sirina2, visina2);
        }
        else {
            curMeta.gornja.set(duzina2, sirina2, visina2);
            curMeta.donja.set(duzina1, sirina1, visina1);
        }


        mete[i] = curMeta;
    }

    oruzje pucac;
    pucac.polozaj = { 1,2,6 };

    // Print tocke i provjera jeli meta pogodena
    for (int i = 0; i < n; i++) {
        bool pogodak = mete[i].pogodak(pucac);
        if (pogodak) {
            cout << "Meta je pogodena" << endl;
        }
        else {
            cout << "Meta nije pogodena" << endl;
        }
        printf("\n Gornja tocka - (%.2f, %.2f, %.2f) \n", mete[i].donja.duzina, mete[i].donja.sirina, mete[i].donja.visina);
        printf("\n Donja tocka - (%.2f, %.2f, %.2f) \n", mete[i].gornja.duzina, mete[i].gornja.sirina, mete[i].gornja.visina);
        cout << "Iduca meta" << endl;
    }





    return 0;
}

